﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Extensions
{
    public static class CollectionExtensions
    {
        public static string ToStringWithSeparator<T>(this IEnumerable<T> collection, string separator = " ")
        {
            if (collection == null || collection.Count() < 0) return string.Empty;

            var stringBuilder = new StringBuilder();
            T[] array = collection.ToArray();

            for (int i = 0; i < array.Length; i++)
            {
                stringBuilder.Append(array[i]);

                if (i < array.Length - 1)
                {
                    stringBuilder.Append(separator);
                }
            }

            return stringBuilder.ToString();
        }
    }
}
