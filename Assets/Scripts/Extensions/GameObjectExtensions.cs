﻿using System;
using UnityEngine;

namespace Assets.Scripts.Extensions
{
    [Serializable]
    public static class GameObjectExtensions
    {
        public static void RemoveCloneFromName(this GameObject gameObject)
        {
            gameObject.name = gameObject.name.Replace("(Clone)", string.Empty);
        }
    }
}
