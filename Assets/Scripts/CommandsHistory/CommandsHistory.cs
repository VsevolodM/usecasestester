﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.CommandsHistory
{
    public class CommandsHistory
    {
        public List<CommandsHistoryItem> Items { get; private set; }

        public CommandsHistory()
        {
            Items = new List<CommandsHistoryItem>();
        }

        public void AddHistoryItem(string commandName, object argument)
        {
            var newHistoryItem = new CommandsHistoryItem(commandName, argument);
            Items.Add(newHistoryItem);
        }

        public string[] GetHistoryStrings()
        {
            var stringsArray = Items.Select(itm => itm.GetStringForHistory()).ToArray();

            for (int i = 0; i < stringsArray.Length; i++)
            {
                stringsArray[i] = $"[{i + 1}] {stringsArray[i]}";
            }

            return stringsArray;
        }
    }
}
