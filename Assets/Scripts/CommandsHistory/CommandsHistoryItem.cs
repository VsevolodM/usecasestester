﻿namespace Assets.Scripts.CommandsHistory
{
    public class CommandsHistoryItem
    {
        public string CommandName { get; private set; }
        public object Argument { get; private set; }

        public CommandsHistoryItem(string commandName, object argument)
        {
            CommandName = commandName;
            Argument = argument;
        }

        public string GetStringForHistory()
        {
            string historyString = $"Execute {CommandName}";;

            if (Argument != null)
            {
                historyString = $"{historyString} with argument {Argument}";
            }

            return historyString;
        }
    }
}
