﻿using Assets.Scripts.CustomLog;
using Assets.Scripts.Helpers;
using UnityEngine;

namespace Assets.Scripts
{
    public class EntryPoint : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            CustomLogger.Subscribe();
            NavigationHelper.OpenMainScreen();
        }
    }
}
