﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UseCasesCommands
{
    [Serializable, CreateAssetMenu(fileName = "WaitRealtimeUseCaseCommand", menuName = "UseCaseCommands/WaitRealtimeUseCaseCommand")]
    public class WaitRealtimeUseCaseCommand : WaitUseCaseCommand
    {
        public override IEnumerator Execute()
        {
            yield return new WaitForSecondsRealtime(waitTime);
        }
    }
}
