﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UseCasesCommands
{
    [Serializable, CreateAssetMenu(fileName = "ButtonDownUseCaseCommand", menuName = "UseCaseCommands/ButtonDownUseCaseCommand")]
    public class ButtonDownUseCaseCommand : BaseUIUseCaseCommand
    {
        protected override void ExecuteEvent()
        {
            ExecuteEvents.Execute(GetUIGameObject(), new PointerEventData(EventSystem.current), ExecuteEvents.pointerDownHandler);
        }
    }
}
