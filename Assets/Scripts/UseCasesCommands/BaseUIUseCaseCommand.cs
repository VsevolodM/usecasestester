﻿using System;
using System.Collections;
using Assets.Scripts.CustomLog;
using UnityEngine;

namespace Assets.Scripts.UseCasesCommands
{
    [Serializable]
    public abstract class BaseUIUseCaseCommand : BaseUseCaseCommand
    {
        [SerializeField] private string uiGameObjectPath;

        public override IEnumerator Execute()
        {
            ExecuteEvent();
            yield return null;
        }

        protected GameObject GetUIGameObject()
        {
            var uiGameObject = GameObject.Find(uiGameObjectPath);

            if (uiGameObject == null)
            {
                UserCasesLogger.LogError($"UI element {uiGameObjectPath} was not found in the scene!");
            }

            return uiGameObject;
        }

        protected abstract void ExecuteEvent();

        public override object GetArgument()
        {
            return uiGameObjectPath;
        }
    }
}
