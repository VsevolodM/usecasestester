﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UseCasesCommands
{
    [Serializable, CreateAssetMenu(fileName = "ButtonUpUseCaseCommand", menuName = "UseCaseCommands/ButtonUpUseCaseCommand")]
    public class ButtonUpUseCaseCommand : BaseUIUseCaseCommand
    {
        protected override void ExecuteEvent()
        {
            var uiGameObject = GetUIGameObject();
            var pointerEventData = new PointerEventData(EventSystem.current);
            ExecuteEvents.Execute(uiGameObject, pointerEventData, ExecuteEvents.pointerUpHandler);
            ExecuteEvents.Execute(uiGameObject, pointerEventData, ExecuteEvents.pointerClickHandler);
        }
    }
}
