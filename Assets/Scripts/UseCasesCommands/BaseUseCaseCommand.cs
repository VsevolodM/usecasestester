﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UseCasesCommands
{
    [Serializable]
    public abstract class BaseUseCaseCommand : ScriptableObject
    {
        public abstract IEnumerator Execute();

        public virtual object GetArgument()
        {
            return null;
        }
    }
}
