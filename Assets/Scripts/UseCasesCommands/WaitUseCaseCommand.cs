﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UseCasesCommands
{
    [Serializable, CreateAssetMenu(fileName = "WaitUseCaseCommand", menuName = "UseCaseCommands/WaitUseCaseCommand")]
    public class WaitUseCaseCommand : BaseUseCaseCommand
    {
        [SerializeField] protected float waitTime = 1f;

        public override IEnumerator Execute()
        {
            yield return new WaitForSeconds(waitTime);
        }

        public override object GetArgument()
        {
            return waitTime;
        }
    }
}
