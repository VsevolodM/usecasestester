﻿using UnityEngine;

namespace Assets.Scripts.Helpers
{
    /// <summary>
    /// Be aware this will not prevent a non singleton constructor
    ///   such as `T myT = new T();`
    /// To prevent that, add `protected T () {}` to your singleton class.
    /// 
    /// As a note, this is made as MonoBehaviour because we need Coroutines.
    /// </summary>
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;

        private static readonly object Lock = new object();

        public static T Instance
        {
            get
            {
                if (ApplicationIsQuitting)
                {
                    return null;
                }

               lock (Lock)
               {
                    if (instance == null)
                    {
                        instance = (T)FindObjectOfType(typeof(T));

                        if (FindObjectsOfType(typeof(T)).Length > 1)
                        {
                            return instance;
                        }

                        if (instance == null)
                        {
                            GameObject singleton = new GameObject(string.Format("{{{0}}}", typeof(T)));
                            instance = singleton.AddComponent<T>();
                            singleton.name = "(singleton) " + typeof(T);

                            GameObject parent = GameObject.Find("[Singletones]");
                            if (parent == null)
                            {
                                parent = new GameObject("[Singletones]");
                            }
                            singleton.transform.SetParent(parent.transform);

                            DontDestroyOnLoad(parent);
                        }
                    }

                    return instance;
                }
            }
        }

        private static bool ApplicationIsQuitting;

        public virtual void OnDestroy()
        {
            ApplicationIsQuitting = true;
        }
    }
}