﻿using Assets.Scripts.UI;
using Assets.Scripts.UI.Screens;

namespace Assets.Scripts.Helpers
{
    public static class NavigationHelper
    {
        public static MainScreen OpenMainScreen()
        {
            return ScreenManager.OpenScreen<MainScreen>();
        }

        public static DebugScreen OpenDebugScreen()
        {
            return ScreenManager.OpenScreen<DebugScreen>();
        }
    }
}
