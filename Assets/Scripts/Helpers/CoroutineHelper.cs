﻿namespace Assets.Scripts.Helpers
{
    public class CoroutineHelper : Singleton<CoroutineHelper>
    {
        public override void OnDestroy()
        {
            base.OnDestroy();
            StopAllCoroutines();
        }
    }
}
