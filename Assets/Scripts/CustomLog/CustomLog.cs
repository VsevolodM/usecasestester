﻿using UnityEngine;

namespace Assets.Scripts.CustomLog
{
    public class CustomLog
    {
        public string LogString { get; private set; }
        public string StackTrace { get; private set; }
        public LogType Type { get; private set; }

        public CustomLog(string logString, string stackTrace, LogType type)
        {
            LogString = logString;
            StackTrace = stackTrace;
            Type = type;
        }

        public override string ToString()
        {
            return $"LogString: {LogString}, StackTrace: {StackTrace}, Type: {Type}";
        }
    }
}
