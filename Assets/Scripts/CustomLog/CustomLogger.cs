﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.CustomLog
{
    public static class CustomLogger
    {
        public static event Action OnLogMessagedReceivedEvent;

        public static List<CustomLog> Logs { get; private set; }

        static CustomLogger()
        {
            Logs = new List<CustomLog>();
        }

        public static void Subscribe()
        {
            Application.logMessageReceived += HandleLog;
        }

        private static void HandleLog(string logString, string stackTrace, LogType type)
        {
            var newCustomLog = new CustomLog(logString, stackTrace, type);
            Logs.Add(newCustomLog);
            OnLogMessagedReceivedEvent?.Invoke();
        }
    }
}
