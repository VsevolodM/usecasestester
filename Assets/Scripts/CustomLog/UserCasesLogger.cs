﻿using Assets.Scripts.CommandRunner;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.CustomLog
{
    public static class UserCasesLogger
    {
        private static string ERROR_LOG_PREFIX = "[UserCase Error]:";
        private static string SUCCESS_LOG_PREFIX = "[UserCase Success]:";

        public static void LogSuccess(CommandsSequence sequence)
        {
            var successStringBuilder = new StringBuilder($"Sequence <b>{sequence.SequenceName}</b> was successfully executed!\n");
            var commandListString = GetCommandsTillIndex(sequence, sequence.Commands.Length);
            successStringBuilder.Append(commandListString);
            Debug.Log($"<color=green><b>{SUCCESS_LOG_PREFIX}</b></color> {successStringBuilder}\n");
        }

        public static void LogError(string errorMessage)
        {
            Debug.LogError($"<color=red><b>{ERROR_LOG_PREFIX}</b></color> {errorMessage}\n");
        }

        public static void GenerateErrorReport(CommandsSequence sequence, int startIndex, int endIndex, CommandsSequence bannedSequence)
        {
            var errorStringBuilder = new StringBuilder($"Sequence <b>{sequence.SequenceName}</b> failed on zero-based steps {startIndex}-{endIndex}!\n");
            var commandsListString = GetCommandsTillIndex(sequence, endIndex + 1);
            errorStringBuilder.Append(commandsListString);
            errorStringBuilder.Append($"\n\n\tSequence <b>{bannedSequence.SequenceName}</b> is not allowed!");
            LogError(errorStringBuilder.ToString());
        }

        private static string GetCommandsTillIndex(CommandsSequence sequence, int index)
        {
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < index && i < sequence.Commands.Length; i++)
            {
                stringBuilder.Append($"\n\t [Step\t{i}] {sequence.Commands[i].GetType().Name}, <i>{sequence.Commands[i].GetArgument()}</i>");
            }

            return stringBuilder.ToString();
        }
    }
}
