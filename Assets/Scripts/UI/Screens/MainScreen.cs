﻿using Assets.Scripts.Helpers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Screens
{
    public class MainScreen : BaseScreen
    {
        [SerializeField] private Button debugButton;

        protected override void Awake()
        {
            base.Awake();
            Assert.IsNotNull(debugButton);

            bool isDebugBuild = Debug.isDebugBuild;

            debugButton.gameObject.SetActive(isDebugBuild);

            if (isDebugBuild)
            { 
                debugButton.onClick.AddListener(OnDebugButtonClicked);
            }
        }

        private void OnDebugButtonClicked()
        {
            NavigationHelper.OpenDebugScreen();
        }

        private void OnDestroy()
        {
            debugButton.onClick.RemoveListener(OnDebugButtonClicked);
        }
    }
}
