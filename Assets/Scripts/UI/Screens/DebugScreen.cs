﻿using Assets.Scripts.CommandRunner;
using Assets.Scripts.CustomLog;
using Assets.Scripts.Extensions;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Screens
{
    public class DebugScreen : BaseScreen
    {
        [SerializeField] private Button closeButton;
        [SerializeField] private Button executeUseCasesButton;
        [SerializeField] private Text consoleLogText;

        protected override void Awake()
        {
            base.Awake();
            Assert.IsNotNull(closeButton);
            Assert.IsNotNull(executeUseCasesButton);
            executeUseCasesButton.onClick.AddListener(OnExecuteUseCasesButton);
            closeButton.onClick.AddListener(Close);

            RefreshConsoleLogText();
            CustomLogger.OnLogMessagedReceivedEvent += RefreshConsoleLogText;
        }

        private void RefreshConsoleLogText()
        {
            consoleLogText.text = CustomLogger.Logs.Select(l => l.LogString + "\n").ToStringWithSeparator();
        }

        private void OnExecuteUseCasesButton()
        { 
            var commandRunners = FindObjectsOfType<CommandsRunner>();

            for (int i = 0; i < commandRunners.Length; i++)
            {
                commandRunners[i].Run();
            }
        }

        private void OnDestroy()
        {
            executeUseCasesButton.onClick.RemoveListener(OnExecuteUseCasesButton);
            closeButton.onClick.RemoveListener(Close);
            CustomLogger.OnLogMessagedReceivedEvent -= RefreshConsoleLogText;
        }
    }
}
