﻿using UnityEngine;

namespace Assets.Scripts.UI.Screens
{
    public abstract class BaseScreen : MonoBehaviour
    {
        public Canvas Canvas { get; private set; }

        protected virtual void Awake()
        {
            Canvas = GetComponent<Canvas>();
        }

        public void Close()
        {
            ScreenManager.CloseScreen(this);
        }
    }
}
