﻿using Assets.Scripts.Extensions;
using Assets.Scripts.UI.Screens;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public static class ScreenManager
    {
        private static string SCREEN_ROOT_FOLDER = "UI/Screens/";
        private static List<BaseScreen> history;
        private static Transform screensParent;

        private static int currentCanvasOrder = 500;

        static ScreenManager()
        {
            history = new List<BaseScreen>();
            screensParent = new GameObject("[ScreensParent]").transform;
            Object.DontDestroyOnLoad(screensParent.gameObject);
        }
        
        public static T OpenScreen<T>() where T: BaseScreen
        {
            string prefabPath = $"{SCREEN_ROOT_FOLDER}{typeof(T).Name}";
            var screenPrefab = Resources.Load<T>(prefabPath);
            var newScreen = Object.Instantiate(screenPrefab, screensParent).GetComponent<T>();
            newScreen.gameObject.RemoveCloneFromName();
            newScreen.Canvas.worldCamera = Camera.main;
            newScreen.Canvas.sortingOrder = currentCanvasOrder;
            currentCanvasOrder++;
            return newScreen;
        }

        public static void CloseScreen(BaseScreen screen)
        {
            history.Remove(screen);
            Object.Destroy(screen.gameObject);
            currentCanvasOrder--;
        }
    }
}
