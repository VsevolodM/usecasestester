﻿namespace Assets.Scripts.CommandRunner
{
    public struct SubsequenceData
    {
        public int StartIndex { get; private set; }
        public int EndIndex { get; private set; }
        public bool Contains { get; private set; }

        public SubsequenceData(int startIndex, int endIndex, bool contains)
        {
            StartIndex = startIndex;
            EndIndex = endIndex;
            Contains = contains;
        }

        public override string ToString()
        {
            return $"StartIndex: {StartIndex}, EndIndex: {EndIndex}, Contains: {Contains}";
        }
    }
}