﻿using System;
using Assets.Scripts.Extensions;
using Assets.Scripts.UseCasesCommands;
using UnityEngine;

namespace Assets.Scripts.CommandRunner
{
    [Serializable]
    public class BannedSequence
    {
        [SerializeField] private BaseUseCaseCommand[] commands;
        public BaseUseCaseCommand[] Commands => commands;

        public string GetSequenceString()
        {
            return commands.ToStringWithSeparator(" -> ");
        }

        public override string ToString()
        {
            return $"commands: {commands.ToStringWithSeparator()}";
        }
    }
}
