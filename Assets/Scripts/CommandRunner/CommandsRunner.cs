﻿using Assets.Scripts.CustomLog;
using Assets.Scripts.Extensions;
using Assets.Scripts.Helpers;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.CommandRunner
{
    public class CommandsRunner : MonoBehaviour
    {
        [SerializeField] private CommandsSequence[] bannedSequences;
        [SerializeField] private CommandsSequence[] commandsSequences;

        private Coroutine executeCommandsCoroutine;

        public void Run()
        {
            CoroutineHelper.Instance.SafeStopCoroutine(executeCommandsCoroutine);
            executeCommandsCoroutine = CoroutineHelper.Instance.StartCoroutine(ExecuteCommands());
        }

        private IEnumerator ExecuteCommands()
        {
            for (int i = 0; i < commandsSequences.Length; i++)
            {
                for (int j = 0; j < commandsSequences[i].Commands.Length; j++)
                {
                    var currentCommand = commandsSequences[i].Commands[j];
                    yield return CoroutineHelper.Instance.StartCoroutine(currentCommand.Execute());
                }

                CheckForBannedSequences(commandsSequences[i]);
            }

            yield return null;
        }

        private void CheckForBannedSequences(CommandsSequence sequence)
        {
            if (bannedSequences == null || bannedSequences.Length == 0)
            {
                return;
            }

            SubsequenceData subsequenceData = default;

            for (int i = 0; i < bannedSequences.Length; i++)
            {
                subsequenceData = sequence.GetSubsequenceData(bannedSequences[i].Commands);

                if (subsequenceData.Contains)
                {
                    UserCasesLogger.GenerateErrorReport(sequence, subsequenceData.StartIndex, subsequenceData.EndIndex, bannedSequences[i]);
                    break;
                }
            }

            if(!subsequenceData.Contains)
            {
                UserCasesLogger.LogSuccess(sequence);
            }
        }
    }
}