﻿using System;
using System.Linq;
using Assets.Scripts.Extensions;
using Assets.Scripts.UseCasesCommands;
using UnityEngine;

namespace Assets.Scripts.CommandRunner
{
    [Serializable]
    public class CommandsSequence
    {
        [SerializeField] private string sequenceName = "Sequence";
        [SerializeField] private BaseUseCaseCommand[] commands;

        public BaseUseCaseCommand[] Commands => commands;
        public string SequenceName => sequenceName;

        public string GetSequenceString()
        {
            return commands.Select(c => $"<i>{c.GetType().Name}</i>").ToStringWithSeparator(" -> ");
        }

        public SubsequenceData GetSubsequenceData(BaseUseCaseCommand[] otherCommands)
        { 
            for (int i = 0; i < commands.Length; i++)
            {
                int otherCommandsIndex = 0;
                int equalAmount = 0;

                for (int j = i; j < commands.Length && otherCommandsIndex < otherCommands.Length; j++)
                {
                    bool areCommandsEqual = commands[j] == otherCommands[otherCommandsIndex];

                    if (!areCommandsEqual)
                    {
                        break;
                    }
                    else
                    {
                        equalAmount++;

                        if (equalAmount == otherCommands.Length)
                        {
                            return new SubsequenceData(i, j, true);
                        }
                    }

                    otherCommandsIndex++;
                }
            }

            return new SubsequenceData(0, 0, false);
        }
    }
}
